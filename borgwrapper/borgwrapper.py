#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This script is a wrapper around Borg.
It is opinionated as all Borg's CLI arguments are not really parametrizable.
The main purpose is to be able to write Borg's configuration once
in the configuration file and not worry about it anymore.

The script can then be executed by a CRON, and you can receive
a summary mail if SMTP information has been provided in the configuration file.

You can also quickly list the last archives or get their detailed
information without needing to remember Borg CLI details.

The current capabilities are:
- initialize repository
- execute backups
- list archives
- get detailed information about archives
- prune archives
- display repository size / quota
- send summary mail (if SMTP information is provided)
"""
import argparse
from os.path import expanduser
from email.mime.text import MIMEText
import logging
from logging import handlers
import smtplib
import subprocess
import sys
import yaml

HOME = expanduser("~")
CONFIG_FILE = "%s/.borg-config.yaml" % HOME
LOG_FORMATTER = logging.Formatter(
    "%(asctime)s [%(levelname)s] %(message)s", datefmt='%Y-%m-%d %H:%M:%S')

# Default logger
_logger = logging.getLogger(__name__)
ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(LOG_FORMATTER)
_logger.addHandler(ch)

# Mail logger, only used for messages worth
# sending by mail
_mail_logger = logging.getLogger('mail_logger')
_mail_logger.setLevel(logging.INFO)
nh = logging.NullHandler()
nh.setLevel(logging.INFO)
_mail_logger.addHandler(
    nh
)  # Avoid complaints "No handlers could be found" if mails are deactivated


def load_config():
    """
    Parse the configuration file.
    Returns a dict.
    """
    try:
        with open(CONFIG_FILE, 'r') as stream:
            return yaml.load(stream)
    except yaml.YAMLError as exc:
        raise exc
    except IOError as exc:
        raise exc


def get_config_values_or_raise(keys, config):
    """
    Get value(s) in the given configuration dict based on the provided
    key(s).
    Raise an exception if a key is not found in the configuration.
    If the keys argument is a string, returns the only value as a string.
    If the keys argument is a list or tuple, return a list of values.
    """
    if not isinstance(keys, (list, tuple)):
        keys = [keys]
    values = []
    for k in keys:
        if not config.get(k, False):
            raise ConfigError('%s not found in configuration' % k)
        values.append(config.get(k))
    return values[0] if len(values) == 1 else values


def log_process_output(stdout, stderr):
    """
    Log the outputs returned by a subprocess.
    """
    if stderr:
        _logger.error("Errors:")
        _logger.error(stderr)
        _mail_logger.error("Errors:")
        _mail_logger.error(stderr)
    _logger.info(stdout)
    _mail_logger.info(stdout)


def setup_email(main_config):
    """
    Get mail config from main configuration,
    and if not empty, create a new MailHandler
    and add it to the mail logger.
    """
    mail_config = main_config.get('email', None)
    if mail_config:
        mail_handler = MailHandler(config=mail_config)
        mail_handler.setLevel(logging.INFO)
        mail_handler.setFormatter(LOG_FORMATTER)
        _mail_logger.addHandler(mail_handler)
    else:
        _logger.warn("No SMTP configuration found, no e-mail will be sent.")


def postprocesses(config):
    """
    Execute the post-processes defined in the configuration.
    """
    for postprocess in config:
        proc = subprocess.Popen(
            postprocess,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True,
            shell=True)
        stdout, stderr = proc.communicate()
        log_process_output(stdout, stderr)


class ConfigError(Exception):
    """
    Base error class for configuration issues.
    """
    pass

class ArgsError(Exception):
    """
    Base error class for borg additional arguments issues.
    """
    pass


class Borg:
    def __init__(self, config, args=None):
        self.config = config
        if not config:
            raise ConfigError("No configuration specified.")

        # Prefix of the archives.
        # Important when pruning, to prune the archives having the same prefix,
        # for instance if multiple machines are using the same repository.
        # Defaults to Borg's {hostname} variable.
        self.prefix = self.config.get('prefix', '{hostname}')
        if args and not isinstance(args, (list, tuple)):
            raise ArgsError("Additional arguments must be list or tuple.")

        # Prepare additional args.
        # Split them on space and add them in the list.
        # For instance, if args is:
        #   ['--last 2', '--dry-run']
        # self.args will be:
        #   ['--last', 2, '--dry-run']
        # So it can be added to the subprocess command.
        self.args = []
        for arg_value in args:
            self.args += arg_value.strip().split()

    def get_cmd(self):
        """
        Create the Borg command.
        Return a list that can be used by subprocess.Popen.
        """
        cmd = [get_config_values_or_raise('cmd', self.config)]
        if self.config.get('remote_path', False):
            cmd.append('--remote-path')
            cmd.append(self.config.get('remote_path'))
        return cmd

    def init(self):
        _logger.info("Initializing Borg repository")
        _mail_logger.info("Initializing Borg repository")
        cmd = self.get_cmd()
        repository = get_config_values_or_raise('repository', self.config)
        cmd.append('init')
        cmd.append('--encryption')
        cmd.append('keyfile')
        cmd.append(repository)

        _logger.debug("cmd: %s", cmd)
        # Redirect stderr to stdout because Borg is weird, see:
        # https://github.com/borgbackup/borg/issues/520
        proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        stdout, stderr = proc.communicate()
        log_process_output(stdout, stderr)
        if proc.returncode > 0:
            _logger.error("Process terminated with error code %s",
                          proc.returncode)
            _mail_logger.error("Process terminated with error code %s",
                               proc.returncode)
            raise Exception(
                "Process terminated with error code %s" % proc.returncode)

        _logger.info("Borg repository initialized")
        _logger.info("!! Please backup the repokey !! It should be located\
        in your home directory under ~/.config/borg/keys")
        _mail_logger.info("Borg repository initialized")

    def backup(self):
        # pylint: disable=E0632
        _logger.info("Start backup")
        _mail_logger.info("Start backup")
        cmd = self.get_cmd()
        (source_directories, repository) = get_config_values_or_raise(
            ('source_directories', 'repository'), self.config)
        cmd.append('create')
        cmd.append('--verbose')
        cmd.append('--filter')
        cmd.append('AME')
        cmd.append('--list')
        cmd.append('--stats')
        cmd.append('--show-rc')
        cmd.append('--compression')
        cmd.append('lz4')
        cmd.append('--exclude-caches')
        cmd += self.args

        exclusions = self.config.get('exclude', None)
        for exclusion in exclusions:
            cmd.append('--exclude')
            cmd.append(exclusion)

        cmd.append("%s::%s-{now}" % (repository, self.prefix))
        for source_directory in source_directories:
            cmd.append(source_directory)

        _logger.debug("Backup cmd: %s", cmd)
        proc = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True)

        while True:
            line = proc.stdout.readline().strip()
            if line == '' and proc.poll() is not None:
                break
            _logger.info(line)

        _logger.info("End backup. Finished with code %s." % proc.returncode)
        _mail_logger.info(
            "End backup. Finished with code %s." % proc.returncode)
        if proc.returncode > 0:
            _logger.error("Process terminated with error code %s",
                          proc.returncode)
            _mail_logger.error("Process terminated with error code %s",
                               proc.returncode)
            raise Exception(
                "Process terminated with error code %s" % proc.returncode)

    def list(self, last=None):
        """
        List the archives of the repository.
        """
        # pylint: disable=E0632
        _logger.info("Listing archives")
        _mail_logger.info("Listing archives")
        cmd = self.get_cmd()
        repository = get_config_values_or_raise('repository', self.config)
        cmd.append('list')
        cmd += self.args
        cmd.append(repository)

        if last:
            cmd.append('--last')
            cmd.append(str(last))

        _logger.debug("cmd: %s", cmd)
        # Redirect stderr to stdout because Borg is weird, see:
        # https://github.com/borgbackup/borg/issues/520
        proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        stdout, stderr = proc.communicate()
        log_process_output(stdout, stderr)
        if proc.returncode > 0:
            _logger.error("Process terminated with error code %s",
                          proc.returncode)
            _mail_logger.error("Process terminated with error code %s",
                               proc.returncode)
            raise Exception(
                "Process terminated with error code %s" % proc.returncode)

    def info(self, archive=None, last=None):
        """
        Print detailed information about the repository (if archive is not specified),
        or about a specific archive (if archive is specified).
        Last considers only last N archives.
        """
        # pylint: disable=E0632
        _logger.info("Archive info")
        _mail_logger.info("Archive info")
        cmd = self.get_cmd()
        repository = get_config_values_or_raise('repository', self.config)
        cmd.append('info')
        cmd += self.args

        if archive:
            repository += "::%s" % archive

        cmd.append(repository)

        if last:
            cmd.append('--last')
            cmd.append(str(last))

        _logger.debug("cmd: %s", cmd)
        # Redirect stderr to stdout because Borg is weird, see:
        # https://github.com/borgbackup/borg/issues/520
        proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout, stderr = proc.communicate()
        log_process_output(stdout, stderr)
        if proc.returncode > 0:
            _logger.error("Process terminated with error code %s",
                          proc.returncode)
            _mail_logger.error("Process terminated with error code %s",
                               proc.returncode)
            raise Exception(
                "Process terminated with error code %s" % proc.returncode)

    def check(self, archive=None, last=None):
        """
        Check the repository and all the archives (if archive is not specified),
        or a specific archive (if archive is specified).
        Last considers only last N archives.
        """
        # pylint: disable=E0632
        _logger.info("Repo/Archive check")
        _mail_logger.info("Repo/Archive check")
        cmd = self.get_cmd()
        repository = get_config_values_or_raise('repository', self.config)
        cmd.append('check')
        cmd.append('--info')
        cmd += self.args

        if archive:
            repository += "::%s" % archive

        cmd.append(repository)

        if last:
            cmd.append('--last')
            cmd.append(str(last))

        _logger.debug("cmd: %s", cmd)
        # Redirect stderr to stdout because Borg is weird, see:
        # https://github.com/borgbackup/borg/issues/520
        proc = subprocess.Popen(
            cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        stdout, stderr = proc.communicate()
        log_process_output(stdout, stderr)
        if proc.returncode > 0:
            _logger.error("Process terminated with error code %s",
                          proc.returncode)
            _mail_logger.error("Process terminated with error code %s",
                               proc.returncode)
            raise Exception(
                "Process terminated with error code %s" % proc.returncode)

    def prune(self):
        """
        Prune old archives.
        """
        # pylint: disable=E0632
        _logger.info("Prune old archives")
        _mail_logger.info("Prune old archives")
        cmd = self.get_cmd()
        repository = get_config_values_or_raise('repository', self.config)
        cmd.append('prune')
        cmd.append('--prefix')
        cmd.append('%s-' % self.prefix)
        cmd.append('--show-rc')
        cmd.append('--verbose')
        cmd.append('--list')
        cmd += self.args

        # Retention rules.
        # The rules are applied from secondly to yearly,
        # and backups selected by previous rules do not count towards those
        # of later rules. The time that each backup starts is used
        # for pruning purposes.

        # Keep the latest backup on each day,
        # up to 7 most recent days with backups
        # (days without backups do not count).
        cmd.append('--keep-daily')
        cmd.append('7')

        # Keep 4 additional end of week archives.
        cmd.append('--keep-weekly')
        cmd.append('4')

        # Keep 6 additional end of month archives.
        cmd.append('--keep-monthly')
        cmd.append('6')
        cmd.append(repository)

        _logger.debug(cmd)
        # Redirect stderr to stdout because Borg is weird, see:
        # https://github.com/borgbackup/borg/issues/520
        proc = subprocess.Popen(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            universal_newlines=True)
        stdout, stderr = proc.communicate()
        log_process_output(stdout, stderr)
        if proc.returncode > 0:
            _logger.error("Process terminated with error code %s",
                          proc.returncode)
            _mail_logger.error("Process terminated with error code %s",
                               proc.returncode)
            raise Exception(
                "Process terminated with error code %s" % proc.returncode)


class MailHandler(handlers.BufferingHandler):
    """
    Custom MailHandler based on BufferingHandler.
    Only flushes when buffer capacity is reached.
    Flushing sends a mail containing the buffered stream in the body.
    """

    def __init__(self, config=None):
        super(MailHandler, self).__init__(capacity=1024)
        self.config = config
        if not config:
            raise ConfigError("No configuration specified.")

        self.email_from = get_config_values_or_raise('from', self.config)
        self.email_to = get_config_values_or_raise('to', self.config)
        self.smtp_host = get_config_values_or_raise('smtp_host', self.config)
        self.smtp_port = get_config_values_or_raise('smtp_port', self.config)
        self.smtp_password = get_config_values_or_raise(
            'smtp_password', self.config)

    def flush(self):
        """
        Send an email which body will contain the buffered stream,
        then zap the buffer to empty.
        """
        self.acquire()
        try:
            if not self.buffer:
                return

            body = str()
            for record in self.buffer:
                body = body + self.format(record) + "\r\n"
            msg = MIMEText(body.encode('utf-8'), _charset="utf-8")
            subject = 'Borg Backups'
            header = 'To:' + self.email_to + '\n' + 'From: ' + self.email_from + '\n' + 'Subject:' + subject + ' \n'
            msg = header + '\n' + body + '\n\n'

            server = smtplib.SMTP_SSL(self.smtp_host, int(self.smtp_port))
            server.ehlo()
            server.login(self.email_from, self.smtp_password)
            server.sendmail(self.email_from, self.email_to, msg)
            server.close()

            print 'Email sent!'
            self.buffer = []
        except Exception, exc:
            _logger.error("Error during mail sending")
            _logger.error(exc)
        finally:
            self.release()


def run(args, unknown_args):
    if args.verbose:
        _logger.setLevel(logging.DEBUG)
    else:
        _logger.setLevel(logging.INFO)
    try:
        main_config = load_config()
    except Exception as exc:
        print exc
        raise

    if args.email:
        setup_email(main_config)

    # Instantiate a Borg object with only the borg part of the configuration
    borg = Borg(
        main_config.get('borg', None), args=unknown_args)
    if args.init:
        borg.init()
    if args.backup:
        borg.backup()
    if args.list:
        borg.list()
    if args.info:
        borg.info()
    if args.check:
        borg.check()
    if args.prune:
        borg.prune()
    if args.post_processes:
        postprocesses_config = main_config.get('postprocesses', None)
        if postprocesses_config:
            postprocesses(postprocesses_config)


def main():
    epilog_text = "All other arguments will be passed to the borg command."
    parser = argparse.ArgumentParser(epilog=epilog_text)
    parser.add_argument(
        '--init', action='store_true', help="Initializes the repository")
    parser.add_argument(
        '--backup',
        '-b',
        action='store_true',
        help="Execute a backup (borg create)")
    parser.add_argument(
        '--list',
        '-l',
        action='store_true',
        help="List the archives (borg list)")
    parser.add_argument(
        '--info',
        '-i',
        action='store_true',
        help="Get archives information (borg info)")
    parser.add_argument(
        '--check',
        '-c',
        action='store_true',
        help="Check repository and/or archives (borg check)")
    parser.add_argument(
        '--prune',
        '-p',
        action='store_true',
        help="Prune archives (borg prune)")
    parser.add_argument(
        '--post-processes',
        '-pp',
        action='store_true',
        help="Run defined post-processes")
    parser.add_argument(
        '--email',
        '-e',
        action='store_true',
        help="Send a summary E-mail after the actions have been executed")
    parser.add_argument(
        '--verbose', '-v', action='store_true', help="Verbose logging")
    args, unknown_args = parser.parse_known_args()
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(0)
    run(args, unknown_args)


if __name__ == '__main__':
    main()
