from setuptools import setup, find_packages

setup(name='borgwrapper',
      version='0.2',
      description='Simple wrapper around Borg Backup',
      url='https://gitlab.com/lefeverd/borgwrapper',
      author='Lefever David',
      author_email='lefever.d@gmail.com',
      license='MIT',
      packages=find_packages(),
      entry_points={
        'console_scripts': [
            'borgwrapper=borgwrapper:main',
        ],
      },
      install_requires=[
        'PyYAML',
      ],
      zip_safe=False)
