# BorgWrapper

This script is a simple wrapper around Borg ([https://borgbackup.readthedocs.io/en/stable/](https://borgbackup.readthedocs.io/en/stable/)).

The main goal was to automate backups using Borg, by installing the script once and forget about it.  
Because monitoring is important, a mail is sent once the backup is finished, containing some information about the backups.

## DISCLAIMER
**Use it at your own risk !  
We will not be responsible for any data corruption, loss or whatever.**

## Installation

To install the script, you can use pip:

```
pip install git+https://gitlab.com/lefeverd/borgwrapper
```

## Configuration

The script takes its configuration from a file in your home directory: ~/.borg-config.yaml.  
You will find below the supported options.  
You can check Borg's documentation regarding the repository format.

```
borg:
  # path to the borg binary
  cmd: /usr/local/bin/borg
  # repository URL.
  repository: /path/to/repo
  # archive name prefix. Defaults to Borg's {hostname}.
  # important to limit prune to archives with the same prefix.
  prefix: myprefix
  # Optional path of borg on the remote server
  remote_path: /usr/local/bin/borg1/borg1
  # Directories to be backed up
  source_directories:
    - /Users/youruser/somefolder
    - /Users/youruser/otherfolder
  # Exclusions, see borg help patterns 
  exclude:
    - /Users/youruser/somefolder/secret
    - /Users/youruser/somefolder/supersecret
    - "*/.test/"

# Post-process commands to be executed.
# For instance, get rsync.net's quota.
# Useful to get more information in the mail.
postprocesses:
  - ssh -t 12345@server.rsync.net "quota"
  - du -hs /path/to/repo

# If defined, an email will be sent after the backup
email:
  from: you@gmail.com
  to: you@gmail.com
  smtp_host: smtp.gmail.com
  smtp_port: 465
  # If using Gmail, you can generate application
  # specific tokens
  smtp_password: yourpassword
```

## Cron

### Unix

You can use crontab to launch the script.  
I have no example because I did not use the script on a Unix system yet,
but you can edit the cron jobs with:

    crontab -e

And add something like to execute a backup everyday at midnight:

    0 0 * * * /usr/local/bin/borgwrapper -blipe -pp >> /var/log/borg/borg.log

### OSX

On OSX, you can create a LaunchAgent:

    vi ~/Library/LaunchAgents/com.borgwrapper.borg.plist

And paste the following, to execute a backup everyday at midnight:

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>com.borgwrapper.borg</string>
    <key>ProgramArguments</key>
    <array>
        <string>/usr/local/bin/borgwrapper</string>
        <string>-b</string>
        <string>-l</string>
        <string>-i</string>
        <string>-p</string>
        <string>-pp</string>
        <string>-e</string>
    </array>
    <key>StartCalendarInterval</key>
    <dict>
        <key>Hour</key>
        <integer>00</integer>
        <key>Minute</key>
        <integer>00</integer>
    </dict>
    <key>StandardErrorPath</key>
    <string>/Users/youruser/borg.err.log</string>
    <key>StandardOutPath</key>
    <string>/Users/youruser/borg.out.log</string>
</dict>
</plist>
```
